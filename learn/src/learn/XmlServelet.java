package learn;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.labs.repackaged.org.json.HTTP;
import com.google.apphosting.utils.remoteapi.RemoteApiPb.Request;

@SuppressWarnings("serial")
public class XmlServelet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		String userName = req.getParameter("userName");
		HttpSession session = req.getSession();
		if (userName != "" && userName != null) {
			session.setAttribute("userName", userName);
		}
		
			out.println("Hello From Get "+ userName);
			out.println("Session Parameter "+ (String)session.getAttribute("userName"));
		
		
		  
		
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		String userName = req.getParameter("userName");
		String fName = req.getParameter("fName");
		out.println("Hello From Post "+ userName+ " " + fName);
		String status = req.getParameter("status");
		out.println(status);
		//String location = req.getParameter("location");
		String[] location = req.getParameterValues("location");
		for(int i=0; i<location.length; i++){
		
		out.println(location[i]);
		}
	}

}
